% Atelier Intro CTF ![](images/ageei.png){ width=25px }
% Barberousse
% 15 Novembre 2018

## C'est quoi un CTF?


# Catégories communes

## Reverse Engineering

Rétro-Ingénierie de code ou d'un programme compilé

### Challenges correspondants

 - 1337 h4x0r reverse engineering
 - 64 bits Cryptominer 5.0

---

## Crypto

Cryptanalyse

---

## Forensics

Retrouver des informations à partir de traces ou dans des fichiers

### Challenges correspondants

 - Wiresharknado
 - Secure Uncrackable Blockchain Zip Encryption

---

## Stéganographie

Des infos cachées dans quelque chose d'autre pour cacher l'existence de ces infos.

### Challenges correspondants

 - Deal with the devil

---

## Web

### Challenges correspondants

 - Commonwealth Alliance for Computer Assisted Surveillance
 - TranEquiUnionFax

---

## Pwn/Exploitation Binaire

Analyser un programme puis exploiter ses failles

# Outils utilisés dans l'atelier

Ce sont tous des outils Libres et gratuits!

![](images/clibre.png){ width=100px }

[Comité Logiciel Libre](http://clibre.uqam.ca/)

---

## Shell

 - objdump:	Informations sur un fichier objet compilé
 - grep
 - strings
 - curl
 - scripting

---

## Wireshark

 - Analyse de paquets avec un GUI

---

## GDB/GEF

 - GDB: Débugger par défaut du projet GNU
 - GDB Enhanced Features: Rend gdb plus beau et plus utile pour l'analyse binaire

---

## Foremost/Binwalk

 - Foremost: Outil forensique de découpage de fichiers
 - Binwalk: Outil pour l'analyse de firmware et de fichiers embedded

---

## John The Ripper

 - Crack des mots de passe hachés

# Questions?

### @barberousse sur slack

### @CycleOfTheAbsurd sur Gitlab/hub

### competition@ageei.org
	Dérangez Corinne et il me transférera ça

# Remerciements

 - @corinnep pour la logistique de l'atelier
 - @fob pour avoir corrigé des bugs dans mes challenges et pour gérer l'infra CTF de l'AGEEI en général
 - @privat, @nic-lovin et @klammydia pour m'avoir introduit au merveilleux monde de la sécu

# Good Job!

![](images/ageei.png){ width=125px }
