.PHONY: pdfs
.DEFAULT_GOAL := pdfs

pdfs:
	mkdir -p pdfs
	pandoc indices.md -o pdfs/indices.pdf -t beamer
	pandoc presentation.md -o pdfs/presentation.pdf -t beamer

clean:
	rm -rf pdfs

